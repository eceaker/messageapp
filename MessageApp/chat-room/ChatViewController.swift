//
//  ChatViewController.swift
//  MessageApp
//
//  Created by Merih Ece Aker on 3.09.2019.
//  Copyright © 2019 Merih Ece Aker. All rights reserved.
//

import UIKit
import CoreData

class ChatViewController: UITableViewController, ChatInputViewDelegate {
    
    var chatMessageList = [ChatMessage]()
    var cellHeights: [IndexPath : CGFloat] = [:]
    
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib.init(nibName: ChatMessageCell.identifier, bundle: nil), forCellReuseIdentifier: ChatMessageCell.identifier)
        tableView.keyboardDismissMode = .interactive
        
        guard let url = URL(string: "https://jsonblob.com/api/jsonBlob/4f421a10-5c4d-11e9-8840-0b16defc864d") else {
            return
        }
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data, error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                return
            }
            do {
                //here dataResponse received from a network request
                let jsonResponse = try JSONSerialization.jsonObject(with: dataResponse, options: [])
                print(jsonResponse) //Response result
                
                guard let jsonArray = jsonResponse as? [[String: Any]] else {
                    return
                }
                
                var model = [ChatMessage]() //Initialising Model Array
                for dic in jsonArray{
                    model.append(ChatMessage(dic)) // adding now value in Model array
                }
                
                DispatchQueue.main.async {
                    self.chatMessageList.append(contentsOf: model)
                    self.tableView.reloadData()
                    self.tableView.scrollToLast()
                }
                
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        task.resume()
    }
    
    override var canBecomeFirstResponder : Bool {
        return true
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        // fix: swipe back yaparken tableview setContentInsetsAutomatically bozuluyor.
        resignFirstResponder()
        becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        deleteData()
    }
    
    var initialRealKeyboardHeight: CGFloat? = nil
    var initialAccessoryHeight: CGFloat? = nil
    var initialContentOffset: CGPoint? = nil
    
    var wasKeyboardShown = false
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let endFrameY = endFrame?.origin.y ?? 0
            let duration:TimeInterval = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIView.AnimationOptions = UIView.AnimationOptions(rawValue: animationCurveRaw)
            if endFrameY >= UIScreen.main.bounds.size.height {
                return
            }
            
            if let keyboardHeight = endFrame?.height, let inputAccessoryHeight = inputAccessoryView?.bounds.height,
                // check if keyboard was really called not accessory view
                (keyboardHeight > inputAccessoryHeight) {
                
                initialRealKeyboardHeight = initialRealKeyboardHeight ?? (keyboardHeight - inputAccessoryHeight)
                initialContentOffset = initialContentOffset ?? tableView.contentOffset
                initialAccessoryHeight = initialAccessoryHeight ?? inputAccessoryHeight
                
                if let keyboardRealHeight = initialRealKeyboardHeight, var offsetY = initialContentOffset?.y {
                    
                    if wasKeyboardShown, let accessoryHeight = initialAccessoryHeight  {
                        offsetY += (keyboardHeight - accessoryHeight)
                    } else {
                        offsetY += keyboardRealHeight
                    }
                    wasKeyboardShown = true
                    
                    UIView.animate(
                        withDuration: duration,
                        delay: TimeInterval(0),
                        options: animationCurve,
                        animations: {
                            self.tableView?.contentOffset = CGPoint(x: 0, y: offsetY)
                    },
                        completion: nil)
                    
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        initialRealKeyboardHeight = nil
        initialAccessoryHeight = nil
        initialContentOffset = nil
        wasKeyboardShown = false
    }
    
    lazy var mInputAccessoryView: ChatInputView = {
        let chatInputView = ChatInputView()
        chatInputView.delegate = self
        chatInputView.clipsToBounds = true
        return chatInputView
    }()
    
    override var inputAccessoryView: UIView? {
        get {
            return mInputAccessoryView
        }
    }
    
    func willSendMessage(text: String) -> Bool {
        
        let chatMessage = ChatMessage()
        chatMessage.text = text
        chatMessage.timestamp = Date().millisecondsSince1970
        chatMessage.user = user
        
        chatMessageList.append(chatMessage)
        tableView.insertRows(at: [IndexPath(row: chatMessageList.count-1, section: 0)], with: .fade)
        tableView.scrollToLast()
        
        return true
    }
    
    
    
    func deleteData(){
        guard let userID = user?.id else { return }
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AppUser")
        fetchRequest.predicate = NSPredicate(format: "id = %@", NSNumber(value: userID))
        
        do
        {
            let test = try managedContext.fetch(fetchRequest)
            
            let objectToDelete = test[0] as! NSManagedObject
            managedContext.delete(objectToDelete)
            
            do{
                try managedContext.save()
            }
            catch
            {
                print(error)
            }
            
        }
        catch
        {
            print(error)
        }
    }
}

extension ChatViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatMessageList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ChatMessageCell", for: indexPath) as? ChatMessageCell  else {
            fatalError("The dequeued cell is not an instance of GroupTableViewCell.")
        }
        
        let message = chatMessageList[indexPath.row]
        
        cell.messageLabel?.text = message.text
        
        cell.imageURL = message.user?.avatarURL
        cell.nameLabel.text = message.user?.name
        
        let isFriend = (user?.id != message.user?.id)
        cell.isFriend = isFriend
        
        // check if previous message owner is the same person
        var isInfoStackHidden = false
        if indexPath.row > 0 {
            let prevMessage = chatMessageList[indexPath.row - 1]
            if (prevMessage.user?.id == message.user?.id) {
                isInfoStackHidden = true
            }
        }
        cell.infoStackView.isHidden = isInfoStackHidden
        
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cellHeights[indexPath] = cell.frame.size.height
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height =  self.cellHeights[indexPath] {
            return height
        }
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 4
    }
    
}
