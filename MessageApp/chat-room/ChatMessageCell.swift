//
//  ChatMessageCell.swift
//  MessageApp
//
//  Created by Merih Ece Aker on 3.09.2019.
//  Copyright © 2019 Merih Ece Aker. All rights reserved.
//

import UIKit
import Kingfisher

class ChatMessageCell: UITableViewCell {
    
    static let identifier = String.init(describing: ChatMessageCell.self)

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = .clear
        selectionStyle = .none
        
        cardView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(recipientConstrains)
    }
    
    var imageURL: String? {
        didSet {
            if let imageURL = imageURL, let url = URL(string: imageURL) {
                avatarView.kf.setImage(with: url)
            } else {
                avatarView.image = nil
            }
        }
    }
    
    lazy var recipientConstrains: [NSLayoutConstraint] = {
        let c = [
            cardView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20),
            cardView.rightAnchor.constraint(lessThanOrEqualTo: contentView.rightAnchor, constant: -60),
        ]
        return c
    }()
    
    lazy var senderConstrains: [NSLayoutConstraint] = {
        let c = [
            cardView.leftAnchor.constraint(greaterThanOrEqualTo: contentView.leftAnchor, constant: 60),
            cardView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -20),
        ]
        return c
    }()
    
    var isFriend: Bool = false {
        didSet {
            if isFriend {
                NSLayoutConstraint.deactivate(senderConstrains)
                NSLayoutConstraint.activate(recipientConstrains)
                
                cardView.backgroundColor = .lightGray
                messageLabel.textColor = .black
                
                nameLabel.textAlignment = .left
            } else {
                NSLayoutConstraint.deactivate(recipientConstrains)
                NSLayoutConstraint.activate(senderConstrains)
                
                cardView.backgroundColor = .blue
                messageLabel.textColor = .white
                
                nameLabel.textAlignment = .right
                infoStackView.addArrangedSubview(self.infoStackView.subviews[0])
            }
        }
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        //
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        // super.setSelected(selected, animated: animated)
    }
    
}

//
/*
fileprivate extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
*/
