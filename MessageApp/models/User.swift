//
//  User.swift
//  MessageApp
//
//  Created by Merih Ece Aker on 4.09.2019.
//  Copyright © 2019 Merih Ece Aker. All rights reserved.
//

import Foundation

class User {
    var id: Int?
    var name: String?
    var avatarURL: String?
    
    init() {
        
    }
    
    init(id: Int?, name: String?, avatarURL: String?) {
        self.id = id
        self.name = name
        self.avatarURL = avatarURL
    }
    
    init(_ dict: [String: Any]) {
        self.id = dict["id"] as? Int
        self.name = dict["name"] as? String
        self.avatarURL = dict["avatarUrl"] as? String
    }
}
