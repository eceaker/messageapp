//
//  FirstVC.swift
//  MessageApp
//
//  Created by Merih Ece Aker on 30.08.2019.
//  Copyright © 2019 Merih Ece Aker. All rights reserved.
//

import UIKit
import CoreData

class MainViewController: UIViewController {

    @IBOutlet weak var txtNickname: UITextField!
    @IBOutlet weak var btnContinue: UIButton!
    
    var user: User?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnContinue.layer.cornerRadius = btnContinue.frame.height / 2
        txtNickname.layer.cornerRadius = txtNickname.frame.height / 2
        
        btnContinue.clipsToBounds = true
        txtNickname.clipsToBounds = true
        
        txtNickname.layer.borderWidth = 0.3
        txtNickname.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        retrieveUser()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let user = user, let controller = segue.destination as? ChatViewController {
            controller.navigationItem.title = user.name
            controller.user = user
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "showChatSegue" {
            
            if txtNickname.text!.count > 1 {
                createUser(withName: txtNickname.text!, id: 3)
                return true
            }
            else {
                showAlert()
            }
        }
        
        return true
    }
    
    func showAlert() {
        let alert = UIAlertController(title: "Warning", message: "Please enter minimum 2 characters", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func createUser(withName name: String, id: Int) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let userEntity = NSEntityDescription.entity(forEntityName: "AppUser", in: managedContext)!
        
        let data = NSManagedObject(entity: userEntity, insertInto: managedContext)
        data.setValue(id, forKey: "id")
        data.setValue(name, forKeyPath: "name")
        // data.setValue("", forKey: "imageURL")
        
        user = User()
        user?.id = id
        user?.name = name
        
        do {
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func retrieveUser() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "AppUser")
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            for data in result as! [NSManagedObject] {
                user = User()
                user?.id = data.value(forKey: "id") as? Int
                user?.name = data.value(forKey: "name") as? String
                performSegue(withIdentifier: "showChatSegue", sender: btnContinue)
            }
        } catch {
            print("Failed")
        }
    }
}
