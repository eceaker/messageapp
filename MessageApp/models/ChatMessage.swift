//
//  ChatMessage.swift
//  MessageApp
//
//  Created by Merih Ece Aker on 4.09.2019.
//  Copyright © 2019 Merih Ece Aker. All rights reserved.
//

import Foundation

class ChatMessage {
    var id: Int?
    var user: User?
    var text: String?
    var timestamp: Int64?
    
    init() {
        
    }
    
    init(id: Int?, user: User?, text: String?, timestamp: Int64?) {
        self.id = id
        self.user = user
        self.text = text
        self.timestamp = timestamp
    }
    
    init(_ dict: [String: Any]) {
        self.id = dict["id"] as? Int
        self.text = dict["text"] as? String ?? ""
        self.timestamp = dict["timestamp"] as? Int64
        
        self.user = User(dict["user"] as! [String: Any])
    }
}
    
