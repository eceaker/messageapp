//
//  UIView+Border.swift
//  MessageApp
//
//  Created by Merih Ece Aker on 4.09.2019.
//  Copyright © 2019 Merih Ece Aker. All rights reserved.
//

import UIKit

extension UIView {
    
    func removeIfSublayerExists(name: String?) {
        if let sublayerName = name, let sublayers = self.layer.sublayers {
            for sublayer in sublayers {
                if sublayer.name == sublayerName {
                    sublayer.removeFromSuperlayer()
                    // return
                }
            }
        }
    }
    
    private func addBorder(color: UIColor, width: CGFloat, rect: CGRect, name: String){
        removeIfSublayerExists(name: name)
        
        let border = CALayer()
        border.name = name
        border.backgroundColor = color.cgColor
        border.frame = rect
        self.layer.addSublayer(border)
    }
    
    func addTopBorder(color: UIColor, width: CGFloat) {
        let rect = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        let name = "topBorder"
        addBorder(color: color, width: width, rect: rect, name: name)
    }
    
    func addRightBorder(color: UIColor, width: CGFloat) {
        let rect = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        let name = "rightBorder"
        addBorder(color: color, width: width, rect: rect, name: name)
    }
    
    func addBottomBorder(color: UIColor, width: CGFloat) {
        let rect = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        let name = "bottomBorder"
        addBorder(color: color, width: width, rect: rect, name: name)
    }
    
    func addLeftBorder(color: UIColor, width: CGFloat) {
        let rect = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        let name = "leftBorder"
        addBorder(color: color, width: width, rect: rect, name: name)
    }
}
