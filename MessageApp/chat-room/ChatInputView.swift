//
//  ChatInputView.swift
//  MessageApp
//
//  Created by Merih Ece Aker on 3.09.2019.
//  Copyright © 2019 Merih Ece Aker. All rights reserved.
//

import UIKit

class ChatInputView: UIView, UITextViewDelegate {
    
    @IBOutlet var contentView: UIView!
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var cardView: CardView!
    
    var delegate: ChatInputViewDelegate? = nil

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView?.addTopBorder(color: UIColor.black.withAlphaComponent(0.25), width: 0.75)
    }
    
    var heightConstraint: NSLayoutConstraint? = nil
    
    func setup(){
        
        Bundle.main.loadNibNamed("ChatInputView", owner: self, options: nil)
        addSubview(contentView)
        
        // This is required to make the view grow vertically
        autoresizingMask = .flexibleHeight
        
        // Setup textView as needed
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView]))
        
        heightConstraint = contentView.heightAnchor.constraint(equalToConstant: 127.5)
 
        textView.isScrollEnabled = false
        textView.delegate = self
        textView.tintColor = .black
        
    }
    
    let maxLines = 5
    
    override var intrinsicContentSize: CGSize {
        let textSize = textView.sizeThatFits(CGSize(width: textView.bounds.width, height: CGFloat.greatestFiniteMagnitude))
        return CGSize(width: bounds.width, height: textSize.height)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard maxLines > 0 else {
            return true
        }
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
        let textAttributes = [NSAttributedString.Key.font : textView.font!]
        
        var textWidth: CGFloat = textView.frame.inset(by: textView.textContainerInset).width
        
        textWidth -= 2.0 * textView.textContainer.lineFragmentPadding
        
        let boundingRect: CGRect = newText.boundingRect(with: CGSize(width: textWidth, height: 0), options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: textAttributes, context: nil)
        
        let numberOfLines = Int(floor(boundingRect.height / textView.font!.lineHeight))
        
        if(numberOfLines <= maxLines){ // do resizing
            
            heightConstraint?.isActive = false
            textView.isScrollEnabled = false
        } else { // stop resizing
            
            heightConstraint?.constant = bounds.height
            heightConstraint?.isActive = true
            textView.isScrollEnabled = true
        }
        
        return true
    }
    
    // MARK: UITextViewDelegate
    func textViewDidChange(_ textView: UITextView) {
        invalidateIntrinsicContentSize()
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        if delegate?.willSendMessage(text: textView.text) ?? false {
            textView.text = ""
        }
    }
    
}

protocol ChatInputViewDelegate {
    
    func willSendMessage(text: String) -> Bool
}
